FROM registry.gitlab.com/mapellidario/template-gitlab-docker-python/collatz_module:latest

COPY main.py /src/

CMD ["python", "/src/main.py"]

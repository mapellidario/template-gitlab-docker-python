
def collatz_generator(n):
    while n >= 1:
        if n == 1:
            yield 1
            n = -1
        elif n % 2  == 0:
            yield n
            n = int (n / 2)
        else:
            yield n
            n = 3*n +1

def collatz_longest(n):
    lenght, longest = 0, 0
    for i in range(n):
        if (l := len(list(collatz_generator(i)))) > lenght:
        # if (l := sum(1 for _ in collatz_func(i))) > longest:  # smaller memory footprint
            longest = i
            lenght = l
    return longest, lenght
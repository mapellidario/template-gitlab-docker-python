from collatz_module.collatz_file import collatz_generator, collatz_longest

import unittest

class TestCollatz(unittest.TestCase):
    def test_collatz_13(self):
        self.assertListEqual(
            list(collatz_generator(13)),
            [13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
        )
    def test_collatz_1e6(self):
        self.assertTupleEqual(collatz_longest(1_000_000), (837799, 525))
        # self.assertEqual(lenght, 325) # wrong

if __name__ == "__main__":
    unittest.main()

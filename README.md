## Example - simple python program distributed with docker

Reference: https://projecteuler.net/problem=14

run with

```
docker run --pull always --rm -it registry.gitlab.com/mapellidario/template-gitlab-docker-python/main
```